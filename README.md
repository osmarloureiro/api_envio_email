
# API de Envio de Email

Esta API permite gerar e enviar notas fiscais em formato PDF via email. A nota fiscal é gerada a partir de um template HTML, incluindo um QR code que redireciona para detalhes adicionais da nota fiscal.

## Sumário

- [Introdução](#introdução)
- [Instalação](#instalação)
- [Configuração](#configuração)
- [Uso](#uso)
- [Endpoints](#endpoints)
- [Contribuindo](#contribuindo)
- [Licença](#licença)

## Introdução

Esta API permite enviar notas fiscais para clientes via email. As notas fiscais são geradas a partir de um template HTML que é preenchido com os detalhes da nota e convertido para PDF. O PDF gerado é então enviado para o email especificado.

## Instalação

Para instalar e configurar a API localmente, siga os passos abaixo:

1. Clone o repositório:
   \`\`\`bash
   git clone https://gitlab.com/osmarloureiro/api_envio_email.git
   cd api_envio_email
   \`\`\`

2. Instale as dependências:
   \`\`\`bash
   npm install
   \`\`\`

## Configuração

1. Adicione um arquivo \`.env\` na raiz do projeto com as seguintes variáveis:
   \`\`\`env
   EMAIL_USER=antonio@ninescgroup.com
   EMAIL_PASS=senha_do_email
   \`\`\`

2. Crie um arquivo HTML template \`invoice.html\` no diretório do projeto com o conteúdo abaixo:

   \`\`\`html
   <!DOCTYPE html>
   <html>
   <head>
     <meta charset="UTF-8">
     <style>
       body { font-family: 'Arial, sans-serif'; margin: 40px; }
       h1 { text-align: center; }
       .invoice-container { max-width: 800px; margin: auto; }
       .invoice-header { display: flex; justify-content: space-between; align-items: center; }
       .invoice-header img { max-height: 50px; }
       .invoice-details { margin-top: 20px; border-collapse: collapse; width: 100%; }
       .invoice-details th, .invoice-details td { border: 1px solid #ddd; padding: 8px; }
       .invoice-details th { background-color: #f2f2f2; }
       .qr-code { text-align: center; margin-top: 20px; }
     </style>
   </head>
   <body>
     <div class="invoice-container">
       <div class="invoice-header">
         <div>
           <h1>Nota Fiscal</h1>
           <p>Data: {{date}}</p>
         </div>
         <div>
           <img src="logo.png" alt="Logo da Empresa">
         </div>
       </div>
       <div class="invoice-client">
         <p><strong>Cliente:</strong> {{client}}</p>
       </div>
       <table class="invoice-details">
         <thead>
           <tr>
             <th>Descrição</th>
             <th>Valor</th>
           </tr>
         </thead>
         <tbody>
           <tr>
             <td>Valor Total</td>
             <td>{{amount}}</td>
           </tr>
         </tbody>
       </table>
       <div class="qr-code">
         <p>Escaneie o QR code para mais detalhes:</p>
         <img src="{{qrCode}}" alt="QR Code">
       </div>
     </div>
   </body>
   </html>
   \`\`\`

## Uso

Para iniciar o servidor, execute:

\`\`\`bash
node app.js
\`\`\`

### Endpoints

#### \`POST /send-invoice\`

Envie uma solicitação para gerar e enviar uma nota fiscal.

- **URL**: \`/send-invoice\`
- **Método**: \`POST\`
- **Corpo da Solicitação**:
  \`\`\`json
  {
    "email": "cliente@example.com",
    "invoice": {
      "id": "12345",
      "client": "Nome do Cliente",
      "date": "2024-07-02",
      "amount": "R$ 100,00"
    }
  }
  \`\`\`

- **Resposta de Sucesso**:
  \`\`\`json
  {
    "message": "Nota fiscal enviada com sucesso!",
    "info": {...}
  }
  \`\`\`

- **Resposta de Erro**:
  \`\`\`json
  {
    "error": "Descrição do erro"
  }
  \`\`\`

## Contribuindo

Contribuições são bem-vindas! Sinta-se à vontade para abrir um problema ou enviar um pull request.

## Licença

Este projeto está licenciado sob a licença MIT. Veja o arquivo [LICENSE](LICENSE) para mais detalhes.

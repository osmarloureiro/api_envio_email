const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const puppeteer = require('puppeteer');
const QRCode = require('qrcode');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3000;

// Middleware
app.use(bodyParser.json());

// Configuração do transportador do Nodemailer
const transporter = nodemailer.createTransport({
  host: 'mail.ninescgroup.com',
  port: 465,
  secure: true, // Use SSL/TLS
  auth: {
    user: 'nome@dominio.com',
    pass: 'suasenha' // Substitua pela senha da sua conta de email
  }
});

// Função para carregar e preencher o HTML
function loadHTMLTemplate(invoice) {
  let html = fs.readFileSync(path.join(__dirname, 'invoice.html'), 'utf8');
  html = html.replace('{{client}}', invoice.client);
  html = html.replace('{{date}}', invoice.date);
  html = html.replace('{{amount}}', invoice.amount);
  html = html.replace('{{qrCode}}', invoice.qrCode);
  return html;
}

// Função para gerar o PDF a partir de um HTML
async function createInvoice(invoice, path) {
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });
  const page = await browser.newPage();

  const htmlContent = loadHTMLTemplate(invoice);

  await page.setContent(htmlContent);
  await page.pdf({ path });

  await browser.close();
}

// Rota para enviar email com nota fiscal em PDF
app.post('/send-invoice', async (req, res) => {
  const { email, invoice } = req.body;

  // Gera o QR Code como base64
  const invoiceLink = `https://seusite.com/detalhes-nota-fiscal/${invoice.id}`;
  const qrCode = await QRCode.toDataURL(invoiceLink);
  invoice.qrCode = qrCode;

  const pdfPath = path.join(__dirname, 'invoice.pdf');
  await createInvoice(invoice, pdfPath);

  const mailOptions = {
    from: 'nome@dominio.com', // Altere para seu email
    to: email,
    subject: 'Sua Nota Fiscal',
    text: 'Em anexo está a sua nota fiscal.',
    attachments: [
      {
        filename: 'invoice.pdf',
        path: pdfPath
      }
    ]
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.status(500).json({ error: error.toString() });
    }
    res.status(200).json({ message: 'Nota fiscal enviada com sucesso!', info });
  });
});

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
